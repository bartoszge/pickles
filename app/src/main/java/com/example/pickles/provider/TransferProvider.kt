package com.example.pickles.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import com.example.pickles.database.TransferDatabase
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.entity.transfer.Transfer.Companion.TABLE_NAME
import com.example.pickles.view.ViewObservable
import com.example.pickles.view.utils.EventListener


class TransferProvider : ContentProvider(), EventListener {

    private val cursor =
        MatrixCursor(arrayOf("_id", "target", "purpose", "amount", "currency", "date", "category"))

    companion object {
        private const val AUTHORITY = "com.example.pickles.provider"
        val TRANSFER_URI: Uri = Uri.parse("content://$AUTHORITY/$TABLE_NAME")
    }

    override fun onCreate(): Boolean {
        val viewObservable = ViewObservable()
        viewObservable.subscribe(this);
        TransferDatabase.loadAll(context!!, viewObservable)
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        if (cursor.count > 0) {
            return cursor
        }
        return null
    }

    override fun getType(uri: Uri): String? {
        TODO("Not yet implemented")
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        TODO("Not yet implemented")
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        TODO("Not yet implemented")
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        TODO("Not yet implemented")
    }

    override fun update(transfers: List<Transfer>) {
        transfers.map {
            cursor.newRow()
                .add("_id", it.id)
                .add("target", it.target)
                .add("purpose", it.purpose)
                .add("amount", it.amount)
                .add("currency", it.currency)
                .add("date", it.date)
                .add("category", it.category)
        }
    }
}