package com.example.pickles.view.editorView

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.pickles.MainActivity
import com.example.pickles.R
import com.example.pickles.database.TransferDatabase

class DeletePopupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_popup)

        val noButton: Button = findViewById(R.id.no_delete_button)
        noButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val yesButton: Button = findViewById(R.id.yes_delete_button)
        yesButton.setOnClickListener {
            TransferDatabase.delete(this, intent.getIntExtra("id", 0))
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}