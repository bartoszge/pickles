package com.example.pickles.view.utils

import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.databinding.ActivityMainBinding

interface EventListener {
    fun update(transfers: List<Transfer>)
}