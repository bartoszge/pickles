package com.example.pickles.view.mainView

import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.entity.transfer.Currency
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.utils.EventListener
import java.time.LocalDate


class MonthlyBudgetListener(private val binding: ActivityMainBinding) :
    EventListener {

    override fun update(transfers: List<Transfer>) {

        //TODO make views with different currency
        binding.monthlyBudget.text = String.format("%.2f", measureMonthlyBudget(transfers)).plus(
            Currency.PLN
        )
    }


    private fun measureMonthlyBudget(transfers: List<Transfer>): Double {
        val monthlyBudget: List<Transfer> =
            transfers.filter { it.date.month === LocalDate.now().month };

        return if (monthlyBudget.isNotEmpty()) {
            monthlyBudget.map { it.amount }.reduce { it1, it2 -> it1 + it2 }
        } else {
            1.0
        };
    }
}