package com.example.pickles.view.editorView.element

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.example.pickles.EditorActivity
import com.example.pickles.R
import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.entity.transfer.Target
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.utils.EventListener

class TargetAdapter(editorActivity: EditorActivity) : AdapterView.OnItemSelectedListener {

    var target: Target = Target.values()[0];

    init {
        val spinner: Spinner = editorActivity.findViewById(R.id.editor_target_spinner)
        val adapter =
            ArrayAdapter(editorActivity, android.R.layout.simple_spinner_item, Target.values())
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        target = Target.values()[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}