package com.example.pickles.view.mainView

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pickles.EditorActivity
import com.example.pickles.MainActivity
import com.example.pickles.R
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.editorView.DeletePopupActivity
import java.time.format.DateTimeFormatter

class TransferViewAdapter(private val context: MainActivity) :
    RecyclerView.Adapter<TransferViewAdapter.TransferViewHolder>() {
    lateinit var transfers: List<Transfer>;

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TransferViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.transfer_view, viewGroup, false)

        return TransferViewHolder(context,  view)
    }

    override fun onBindViewHolder(transferViewHolder: TransferViewHolder, position: Int) {
        val transfer: Transfer = transfers[position]
        transferViewHolder.transfer = transfers[position]
        transferViewHolder.amountAndCurrency.text = transfer.getAmountWithCurrency();
        transferViewHolder.purpose.text = transfer.purpose;
        transferViewHolder.date.text = transfer.date.format(DateTimeFormatter.ISO_DATE)
        transferViewHolder.category.text = transfer.category.name
    }

    override fun getItemCount(): Int {
        return transfers.size;
    }

    class TransferViewHolder(private val context: MainActivity, view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener, View.OnLongClickListener {
        var transfer: Transfer? = null;
        val amountAndCurrency: TextView =
            itemView.findViewById(R.id.transfer_view_amount_and_currency)
        val purpose: TextView = itemView.findViewById(R.id.transfer_view_purpose)
        val date: TextView = itemView.findViewById(R.id.transfer_view_date)
        val category: TextView = itemView.findViewById(R.id.transfer_view_category)

        init {
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
        }

        override fun onClick(view: View) {
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("Transfer", transfer)
            context.startActivity(intent)
        }

        override fun onLongClick(v: View?): Boolean {
            val intent = Intent(context, DeletePopupActivity::class.java)
            intent.putExtra("id", transfer?.id)
            context.startActivity(intent)
            return true
        }
    }
}