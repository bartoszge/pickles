package com.example.pickles.view.mainView

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pickles.MainActivity
import com.example.pickles.R
import com.example.pickles.entity.transfer.Target
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.view.utils.EventListener

class TransferViewListener(
    private val context: MainActivity,
    private val target: Target
) : EventListener {
    private val transferAdapter = TransferViewAdapter(context)

    private val view =
        if (target == Target.INCOME)
            R.id.transfer_income_view
        else
            R.id.transfer_expense_view;

    override fun update(transfers: List<Transfer>) {

        transferAdapter.transfers = transfers.filter { it.target === this.target }
        val transferExpenseView =
            this.context.findViewById<RecyclerView>(view)
        transferExpenseView.adapter = transferAdapter
        transferExpenseView.layoutManager = LinearLayoutManager(context);
    }
}