package com.example.pickles.view.mainView

import android.content.ContentResolver
import android.database.Cursor
import com.example.pickles.MainActivity
import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.entity.transfer.Currency
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.provider.TransferProvider.Companion.TRANSFER_URI
import com.example.pickles.view.utils.EventListener
import java.time.LocalDate


class MonthlyBudget(mainActivity: MainActivity, binding: ActivityMainBinding) {
    private val contentResolver: ContentResolver = mainActivity.contentResolver

    init {
        var dateAndAmount: Map<LocalDate, Double> = mapOf()
        val cursor: Cursor? = getTransferCursor()

        if (cursor != null)
            dateAndAmount = createTransfers(cursor)

        binding.monthlyBudget.text =
            String.format("%.2f", measureMonthlyBudget(dateAndAmount)).plus(
                Currency.PLN
            )

    }

    private fun getTransferCursor(): Cursor? {
//      String[] projection = { RawContacts._ID, Contacts.DISPLAY_NAME };
//        String selection = RawContacts . DELETED +"=?";
//        String[] selectionArgs = { Integer.toString(0) };
//        String sortOrder = Contacts . DISPLAY_NAME +" ASC";
        return contentResolver.query(TRANSFER_URI, null, null, null, null);
    }


    private fun createTransfers(cursor: Cursor): Map<LocalDate, Double> {

        var dateAndAmount: Map<LocalDate, Double> = mapOf()

        if (cursor.moveToFirst()) {
            do {
                val date: LocalDate =
                    LocalDate.parse(cursor.getString(cursor.getColumnIndex("date")))
                val amount: Double = cursor.getDouble(cursor.getColumnIndex("amount"))
                dateAndAmount = dateAndAmount.plus(Pair(date, amount))
            } while (cursor.moveToNext())
        }
        cursor.close()

        return dateAndAmount
    }


    private fun measureMonthlyBudget(dateAndAmount: Map<LocalDate, Double>): Double {
        val monthlyBudget: Map<LocalDate, Double> =
            dateAndAmount.filter { it.key.month === LocalDate.now().month };

        return if (monthlyBudget.isNotEmpty()) {
            monthlyBudget.map { it.value }.reduce { it1, it2 -> it1 + it2 }
        } else {
            0.0
        };
    }
}