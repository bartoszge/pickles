package com.example.pickles.view.editorView.element

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.example.pickles.EditorActivity
import com.example.pickles.R
import com.example.pickles.entity.transfer.Category

class CategoryAdapter(editorActivity: EditorActivity) : AdapterView.OnItemSelectedListener {

    var category: Category = Category.values()[0];

    init {
        val spinner: Spinner = editorActivity.findViewById(R.id.editor_category_spinner)
        val adapter =
            ArrayAdapter(editorActivity, android.R.layout.simple_spinner_item, Category.values())
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        category = Category.values()[position];
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}