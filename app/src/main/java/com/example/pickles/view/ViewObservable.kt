package com.example.pickles.view

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.view.utils.EventListener

class ViewObservable {
    private var eventListeners: Set<EventListener> = setOf()

    fun subscribe(eventListener: EventListener) {
        eventListeners = eventListeners.plus(eventListener)
    }

    fun unsubscribe(eventListener: EventListener) {
        eventListeners = eventListeners.minus(eventListener)
    }

    fun unsubscribeAll() {
        eventListeners = setOf()
    }

    fun notifyListener(transfers: List<Transfer>) {
        Log.d(AppCompatActivity.ACTIVITY_SERVICE, "Update listener")
        eventListeners.forEach { it.update(transfers) }
    }
}