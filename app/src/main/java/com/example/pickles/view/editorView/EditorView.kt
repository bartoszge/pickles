package com.example.pickles.view.editorView

import android.widget.TextView
import com.example.pickles.EditorActivity
import com.example.pickles.R
import com.example.pickles.entity.transfer.Currency
import com.example.pickles.entity.transfer.Target
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.editorView.element.CategoryAdapter
import com.example.pickles.view.editorView.element.TargetAdapter
import com.example.pickles.view.utils.EventListener
import java.time.LocalDate
import java.time.format.DateTimeParseException

class EditorView(editorActivity: EditorActivity, private val transfer: Transfer?) {

    private var id: Int? = null;
    private val targetAdapter: TargetAdapter = TargetAdapter(editorActivity);
    private val textPurposeView: TextView = editorActivity.findViewById(R.id.editor_purpose_text)
    private val numberAmountView: TextView = editorActivity.findViewById(R.id.editor_amount_number)
    private val dateView: TextView = editorActivity.findViewById(R.id.editor_date)
    private val categoryAdapter: CategoryAdapter = CategoryAdapter(editorActivity);

    init {
        if (transfer != null) {
            id = transfer.id
            targetAdapter.target = transfer.target
            textPurposeView.text = transfer.purpose
            numberAmountView.text = transfer.amount.toString()
            dateView.text = transfer.date.toString()
            categoryAdapter.category = transfer.category
        } else {
            dateView.text = LocalDate.now().toString()
        }
    }

    fun isValid(): Boolean {
        return textPurposeView.text.isNotBlank() &&
                numberAmountView.text.isNotBlank() &&
                dateView.text.isNotBlank() &&
                try {
                    LocalDate.parse(dateView.text) != null
                } catch (e: DateTimeParseException) {
                    return false
                }
    }

    fun createTransfer(): Transfer {

        return Transfer(
            id = id,
            target = targetAdapter.target,
            purpose = textPurposeView.text.toString(),
            amount = formatAmount(targetAdapter.target).toDouble(),
            currency = Currency.PLN,
            date = LocalDate.parse(dateView.text),
            category = categoryAdapter.category
        )
    }

    private fun formatAmount(target: Target): String {
        return if (target === Target.EXPENSE) {
            String.format("-%.2f", numberAmountView.text.toString().toDouble())
        } else {
            String.format("%.2f", numberAmountView.text.toString().toDouble())
        }
    }
}