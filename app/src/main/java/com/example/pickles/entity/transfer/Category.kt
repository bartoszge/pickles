package com.example.pickles.entity.transfer

enum class Category {
    CAR_AND_TRANSPORT,
    DAILY_EXPANSES,
    HOME,
    ENTERTAINMENT,
    PERSONAL
}