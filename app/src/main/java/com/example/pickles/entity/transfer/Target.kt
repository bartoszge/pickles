package com.example.pickles.entity.transfer

enum class Target {
    INCOME,
    EXPENSE
}