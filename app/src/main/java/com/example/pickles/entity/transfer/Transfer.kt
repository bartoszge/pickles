package com.example.pickles.entity.transfer

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pickles.entity.transfer.Transfer.Companion.TABLE_NAME
import java.io.Serializable
import java.time.LocalDate

@Entity(tableName = TABLE_NAME)
data class Transfer(
    @PrimaryKey(autoGenerate = true)
    var id: Int?,
    @ColumnInfo
    var target: Target,
    var purpose: String,
    var amount: Double,
    var currency: Currency,
    var date: LocalDate,
    var category: Category,
) : Serializable {

    companion object {
        const val TABLE_NAME = "transfer"
    }
    fun getAmountWithCurrency(): String {
        return String.format("%.2f", this.amount).plus(this.currency.name)
    }

    override fun toString(): String {
        return String.format(
            "Target: %s\nPurpose: %s\nAmount: %s\nDate: %s\nCategory: %s\n",
            target.toString(),
            purpose,
            getAmountWithCurrency(),
            date.toString(),
            category.toString()
        )
    }
}