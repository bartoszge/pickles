package com.example.pickles

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.pickles.database.TransferDatabase
import com.example.pickles.databinding.ActivityMainBinding
import com.example.pickles.entity.transfer.Target.EXPENSE
import com.example.pickles.entity.transfer.Target.INCOME
import com.example.pickles.view.ViewObservable
import com.example.pickles.view.mainView.MonthlyBudget
import com.example.pickles.view.mainView.MonthlyBudgetListener
import com.example.pickles.view.mainView.TransferViewListener


class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val mainViewObservable = createMainViewObservable(this)
        TransferDatabase.loadAll(this, mainViewObservable)

        //test ContentProvider!!
//        MonthlyBudget(this, binding)

        val button: Button = findViewById(R.id.add_button)
        button.setOnClickListener {
            val intent = Intent(this, EditorActivity::class.java)
            mainViewObservable.unsubscribeAll()
            startActivity(intent)
        }
    }

    private fun createMainViewObservable(mainActivity: MainActivity): ViewObservable {
        val mainViewObservable = ViewObservable();
        mainViewObservable.subscribe(TransferViewListener(mainActivity, EXPENSE))
        mainViewObservable.subscribe(TransferViewListener(mainActivity, INCOME))
        mainViewObservable.subscribe(MonthlyBudgetListener(binding))
        return mainViewObservable;
    }
}