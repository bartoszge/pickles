package com.example.pickles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.pickles.database.TransferDatabase.Companion.insert
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.editorView.EditorView

class EditorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)

        val transfer: Transfer? = intent.getSerializableExtra("Transfer") as Transfer?
        val editorView = EditorView(this, transfer)

        val cancelButton: Button = findViewById(R.id.cancel_button)
        cancelButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        val shareButton: Button = findViewById(R.id.share_button)
        shareButton.setOnClickListener {
            if (editorView.isValid()) {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, editorView.createTransfer().toString())
                    type = "text/plain"
                }
                val shareIntent = Intent.createChooser(sendIntent, "Transfer")
                startActivity(shareIntent)
            }
        }


        val saveButton: Button = findViewById(R.id.save_button)
        saveButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            if (editorView.isValid()) {
                insert(this, editorView.createTransfer())
                startActivity(intent)
            }
        }
    }
}