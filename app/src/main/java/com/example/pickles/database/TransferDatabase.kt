package com.example.pickles.database

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Database;
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.pickles.entity.transfer.LocalDateConverter
import com.example.pickles.entity.transfer.Transfer
import com.example.pickles.view.ViewObservable
import kotlin.concurrent.thread

private const val DATABASE_FILENAME = "transfers"

@Database(version = 1, entities = [Transfer::class])
@TypeConverters(LocalDateConverter::class)
abstract class TransferDatabase : RoomDatabase() {
    abstract fun transfersDao(): TransferDao

    companion object {
        private fun open(context: Context): TransferDatabase =
            Room.databaseBuilder(
                context,
                TransferDatabase::class.java,
                DATABASE_FILENAME
            ).build()

        fun loadAll(
            context: Context,
            viewObservable: ViewObservable
        ) {
            val db by lazy { open(context) }

            thread {
                Log.d(AppCompatActivity.STORAGE_SERVICE, "Load transfers from database")
                val transfers = db.transfersDao().getAll()
                if (transfers.isNotEmpty()) {
                    viewObservable.notifyListener(transfers)
                }
            }
        }

        fun insert(context: Context, transfer: Transfer) {
            val db by lazy { open(context) }
            thread {
                if (transfer.id != null && db.transfersDao().getById(transfer.id!!) != null)
                    db.transfersDao().update(transfer)
                else
                    db.transfersDao().insert(transfer)
                db.close()
            }
        }

        fun delete(context: Context, id: Int) {
            val db by lazy { open(context) }
            thread {
                db.transfersDao().deleteById(id)
                db.close()
            }
        }
    }

}
