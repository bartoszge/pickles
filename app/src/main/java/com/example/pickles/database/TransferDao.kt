package com.example.pickles.database

import androidx.room.*
import com.example.pickles.entity.transfer.Transfer

@Dao
interface TransferDao {
    @Query("SELECT* FROM Transfer")
    fun getAll(): List<Transfer>

    @Query("SELECT * FROM Transfer WHERE id=:id")
    fun getById(id: Int): Transfer?


    @Query("DELETE FROM Transfer WHERE id = :id")
    fun deleteById(id: Int)

    @Insert
    fun insert(student: Transfer)

    @Update
    fun update(student: Transfer)

    @Delete
    fun delete(student: Transfer)
}